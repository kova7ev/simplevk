# Simple library for easy communication with vk.com #

```
#!Java

private void publishToVk() {
        Photo photo = new Photo(bytes);
        photo.addDescription("I'm so crazy");
        
        SimpleVk.publish(photo, new SimpleVk.OnPublishListener() {
        	@Override
                public void onComplete(String id) {
                	Log.w("API", "onComplete");
                }

                @Override
              	public void onThinking() {
                }

                @Override
                public void onException(Throwable throwable) {
                }

                @Override
                public void onFail(String reason) {
                }
  	});
}
```