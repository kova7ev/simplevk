package com.despair.simple.vk;

/**
 * Created by DESPAIR on 1/14/14.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.perm.kate.api.Auth;

/**
 * This Activity is a necessary part of the overall Facebook login process
 * but is not meant to be used directly. Add this activity to your
 * AndroidManifest.xml to ensure proper handling of Facebook login.
 * <pre>
 * {@code
 * <activity android:name="com.facebook.LoginActivity"
 *           android:theme="@android:style/Theme.Translucent.NoTitleBar"
 *           android:label="@string/app_name" />
 * }
 * </pre>
 * Do not start this activity directly.
 */
public class LoginActivity extends Activity {

    WebView webview;

    static final String RESULT_KEY = "com.facebook.LoginActivity:Result";

    private static final String TAG = LoginActivity.class.getName();
    private static final String NULL_CALLING_PKG_ERROR_MSG =
            "Cannot call LoginActivity with a null calling package. " +
                    "This can occur if the launchMode of the caller is singleInstance.";
    private static final String SAVED_CALLING_PKG_KEY = "callingPackage";
    private static final String SAVED_AUTH_CLIENT = "authorizationClient";
    private static final String EXTRA_REQUEST = "request";

    private String callingPackage;
    //TODO: unimplemented
//    private AuthorizationClient authorizationClient;
//    private AuthorizationClient.AuthorizationRequest request;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        webview = (WebView) findViewById(R.id.vkontakteview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.clearCache(true);

        //Чтобы получать уведомления об окончании загрузки страницы
        webview.setWebViewClient(new VkontakteWebViewClient());

        //otherwise CookieManager will fall with java.lang.IllegalStateException: CookieSyncManager::createInstance() needs to be called before CookieSyncManager::getInstance()
        CookieSyncManager.createInstance(this);

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        String url = Auth.getUrl("4111060", Auth.getSettings());
        webview.loadUrl(url);

        //TODO: unimplemented
//        setContentView(R.layout.com_facebook_login_activity_layout);

//        if (savedInstanceState != null) {
//            callingPackage = savedInstanceState.getString(SAVED_CALLING_PKG_KEY);
            //TODO: unimplemented
//            authorizationClient = (AuthorizationClient) savedInstanceState.getSerializable(SAVED_AUTH_CLIENT);
//        } else {
//            callingPackage = getCallingPackage();
            //TODO: unimplemented
//            authorizationClient = new AuthorizationClient();
//            request = (AuthorizationClient.AuthorizationRequest) getIntent().getSerializableExtra(EXTRA_REQUEST);
//        }
//TODO: unimplemented
//        authorizationClient.setContext(this);
//        authorizationClient.setOnCompletedListener(new AuthorizationClient.OnCompletedListener() {
//            @Override
//            public void onCompleted(AuthorizationClient.Result outcome) {
//                onAuthClientCompleted(outcome);
//            }
//        });
//        authorizationClient.setBackgroundProcessingListener(new AuthorizationClient.BackgroundProcessingListener() {
//            @Override
//            public void onBackgroundProcessingStarted() {
//                findViewById(R.id.com_facebook_login_activity_progress_bar).setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onBackgroundProcessingStopped() {
//                findViewById(R.id.com_facebook_login_activity_progress_bar).setVisibility(View.GONE);
//            }
//        });
    }
//TODO: unimplemented
//    private void onAuthClientCompleted(AuthorizationClient.Result outcome) {
//        request = null;
//
//        int resultCode = (outcome.code == AuthorizationClient.Result.Code.CANCEL) ?
//                RESULT_CANCELED : RESULT_OK;
//
//        Bundle bundle = new Bundle();
//        bundle.putSerializable(RESULT_KEY, outcome);
//
//        Intent resultIntent = new Intent();
//        resultIntent.putExtras(bundle);
//        setResult(resultCode, resultIntent);
//
//        finish();
//    }

    @Override
    public void onResume() {
        super.onResume();

        // If the calling package is null, this generally means that the callee was started
        // with a launchMode of singleInstance. Unfortunately, Android does not allow a result
        // to be set when the callee is a singleInstance, so we log an error and return.
//        if (callingPackage == null) {
//            Log.e(TAG, NULL_CALLING_PKG_ERROR_MSG);
//            finish();
//            return;
//        }
//TODO: unimplemented
//        authorizationClient.startOrContinueAuth(request);
    }

    @Override
    public void onPause() {
        super.onPause();
//TODO: unimplemented
//        authorizationClient.cancelCurrentHandler();
//        findViewById(R.id.com_facebook_login_activity_progress_bar).setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);

//        outState.putString(SAVED_CALLING_PKG_KEY, callingPackage);
        //TODO: unimplemented
//        outState.putSerializable(SAVED_AUTH_CLIENT, authorizationClient);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //TODO: unimplemented
//        authorizationClient.onActivityResult(requestCode, resultCode, data);
    }

    //TODO: unimplemented
//    static Bundle populateIntentExtras(AuthorizationClient.AuthorizationRequest request) {
//        Bundle extras = new Bundle();
//        extras.putSerializable(EXTRA_REQUEST, request);
//        return extras;
//    }

    class VkontakteWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            parseUrl(url);
        }
    }

    private void parseUrl(String url) {
        try {
            if (url == null) {
                return;
            }

            Log.i(TAG, "url=" + url);
            if (url.startsWith(Auth.redirect_url)) {
                if (!url.contains("error=")) {
                    String[] auth = Auth.parseRedirectUrl(url);
                    Intent intent = new Intent();
                    intent.putExtra("token", auth[0]);
                    intent.putExtra("user_id", Long.parseLong(auth[1]));
                    setResult(Activity.RESULT_OK, intent);
                }
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        request = null;
//
//        int resultCode = (outcome.code == AuthorizationClient.Result.Code.CANCEL) ?
//                RESULT_CANCELED : RESULT_OK;
//
//        Bundle bundle = new Bundle();
//        bundle.putSerializable(RESULT_KEY, outcome);
//
//        Intent resultIntent = new Intent();
//        resultIntent.putExtras(bundle);
//        setResult(resultCode, resultIntent);
//
//        finish();
    }
}