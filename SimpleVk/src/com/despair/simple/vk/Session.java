package com.despair.simple.vk;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.*;

/**
 * Created by DESPAIR on 1/14/14.
 */


public class Session {
    /**
     * The logging tag used by Session.
     */
    public static final String TAG = Session.class.getCanonicalName();
    // This is the object that synchronizes access to state and tokenInfo
    private final Object lock = new Object();

    /**
     * The action used to indicate that the active session has been opened. This should
     * be used as an action in an IntentFilter and BroadcastReceiver registered with
     * the android.support.v4.content.LocalBroadcastManager.
     */
    public static final String ACTION_ACTIVE_SESSION_OPENED = "com.facebook.sdk.ACTIVE_SESSION_OPENED";

    /**
     * The action used to indicate that the active session has been closed. This should
     * be used as an action in an IntentFilter and BroadcastReceiver registered with
     * the android.support.v4.content.LocalBroadcastManager.
     */
    public static final String ACTION_ACTIVE_SESSION_CLOSED = "com.facebook.sdk.ACTIVE_SESSION_CLOSED";


    /**
     * The default activity code used for authorization.
     *
     * @see #openForRead(OpenRequest)
     * open
     */


    public static final int DEFAULT_AUTHORIZE_ACTIVITY_CODE = 0xface;
    private static final Object STATIC_LOCK = new Object();
    private static Session activeSession;
    private static volatile Context staticContext;


    private final List<StatusCallback> callbacks;
    private Handler handler;
    private StatusCallback statusCallback;
    private String applicationId;
    private TokenCachingStrategy tokenCachingStrategy;
    private SessionState state;
    private AuthorizationRequest pendingAuthorizationRequest;
    private AuthorizationClient authorizationClient;
    private AccessToken tokenInfo;

    /**
     * Provides asynchronous notification of Session state changes.
     *
     * @see Session#open open
     */
    public interface StatusCallback {
        public void call(Session session, SessionState state, Exception exception);
    }

    public static Session getActiveSession() {
        return activeSession;
    }

    public Session(Context context, String applicationId, TokenCachingStrategy tokenCachingStrategy) {
        this(context, applicationId, tokenCachingStrategy, true);
    }

    static void initializeStaticContext(Context currentContext) {
        if ((currentContext != null) && (staticContext == null)) {
            Context applicationContext = currentContext.getApplicationContext();
            staticContext = (applicationContext != null) ? applicationContext : currentContext;
        }
    }

    Session(Context context, String applicationId, TokenCachingStrategy tokenCachingStrategy,
            boolean loadTokenFromCache) {
        // if the application ID passed in is null, try to get it from the
        // meta-data in the manifest.
        //TODO: Unimplemented
//        if ((context != null) && (applicationId == null)) {
//            applicationId = Utility.getMetadataApplicationId(context);
//        }

        //TODO: Unimplemented
//        Validate.notNull(applicationId, "applicationId");

        initializeStaticContext(context);

        if (tokenCachingStrategy == null) {
            tokenCachingStrategy = new SharedPreferencesTokenCachingStrategy(staticContext);
        }

        this.applicationId = applicationId;
        this.tokenCachingStrategy = tokenCachingStrategy;
        this.state = SessionState.CREATED;
        this.pendingAuthorizationRequest = null;
        //TODO: Unimplemented
        this.callbacks = new ArrayList<StatusCallback>();
        this.handler = new Handler(Looper.getMainLooper());
        Bundle tokenState = loadTokenFromCache ? tokenCachingStrategy.load() : null;
        //TODO: Unimplemented
        if (TokenCachingStrategy.hasTokenInformation(tokenState)) {
//            Date cachedExpirationDate = TokenCachingStrategy
//                    .getDate(tokenState, TokenCachingStrategy.EXPIRATION_DATE_KEY);
//            Date now = new Date();
//
//            if ((cachedExpirationDate == null) || cachedExpirationDate.before(now)) {
//                // If expired or we require new permissions, clear out the
//                // current token cache.
//                tokenCachingStrategy.clear();
//                this.tokenInfo = AccessToken.createEmptyToken(Collections.<String>emptyList());
//            } else {
//                // Otherwise we have a valid token, so use it.
            this.tokenInfo = AccessToken.createFromCache(tokenState);
            this.state = SessionState.CREATED_TOKEN_LOADED;
//            }
        } else {
            this.tokenInfo = AccessToken.createEmptyToken(Collections.<String>emptyList());
        }
    }

    public AccessToken getAccessToken() {
        return tokenInfo;
    }

    /**
     * <p>
     * Sets the current active Session.
     * </p>
     * <p>
     * The active Session is used implicitly by predefined Request factory
     * methods as well as optionally by UI controls in the sdk.
     * </p>
     * <p>
     * It is legal to set this to null, or to a Session that is not yet open.
     * </p>
     *
     * @param session A Session to use as the active Session, or null to indicate
     *                that there is no active Session.
     */
    public static final void setActiveSession(Session session) {
        synchronized (Session.STATIC_LOCK) {
            if (session != Session.activeSession) {
                Session oldSession = Session.activeSession;

                if (oldSession != null) {
                    oldSession.close();
                }

                Session.activeSession = session;

                //TODO: Unimplemented
//                if (oldSession != null) {
//                    postActiveSessionAction(Session.ACTION_ACTIVE_SESSION_UNSET);
//                }
                //TODO: Unimplemented
//                if (session != null) {
//                    postActiveSessionAction(Session.ACTION_ACTIVE_SESSION_SET);
//
//                    if (session.isOpened()) {
//                        postActiveSessionAction(Session.ACTION_ACTIVE_SESSION_OPENED);
//                    }
//                }
            }
        }
    }

    /**
     * Closes the local in-memory Session object, but does not clear the
     * persisted token cache.
     */
    public final void close() {
        synchronized (this.lock) {
            final SessionState oldState = this.state;

            switch (this.state) {
                case CREATED:
                case OPENING:
                    this.state = SessionState.CLOSED_LOGIN_FAILED;
//                    postStateChange(oldState, this.state, new FacebookException(
//                            "Log in attempt aborted."));
                    break;

                case CREATED_TOKEN_LOADED:
                case OPENED:
                case OPENED_TOKEN_UPDATED:
                    this.state = SessionState.CLOSED;
//                    postStateChange(oldState, this.state, null);
                    break;

                case CLOSED:
                case CLOSED_LOGIN_FAILED:
                    break;
            }
        }
    }

    void postStateChange(final SessionState oldState, final SessionState newState, final Exception exception) {
        // When we request new permissions, we stay in SessionState.OPENED_TOKEN_UPDATED,
        // but we still want notifications of the state change since permissions are
        // different now.
        if ((oldState == newState) &&
                (oldState != SessionState.OPENED_TOKEN_UPDATED) &&
                (exception == null)) {
            return;
        }

        if (newState.isClosed()) {
            this.tokenInfo = AccessToken.createEmptyToken(Collections.<String>emptyList());
        }
        synchronized (callbacks) {
            // Need to schedule the callbacks inside the same queue to preserve ordering.
            // Otherwise these callbacks could have been added to the queue before the SessionTracker
            // gets the ACTIVE_SESSION_SET action.
            Runnable runCallbacks = new Runnable() {
                public void run() {
                    for (final StatusCallback callback : callbacks) {
                        Runnable closure = new Runnable() {
                            public void run() {
                                // This can be called inside a synchronized block.
                                callback.call(Session.this, newState, exception);
                            }
                        };

                        runWithHandlerOrExecutor(handler, closure);
                    }
                }
            };
            runWithHandlerOrExecutor(handler, runCallbacks);
        }
        if (this == Session.activeSession) {
            if (oldState.isOpened() != newState.isOpened()) {
                if (newState.isOpened()) {
                    postActiveSessionAction(Session.ACTION_ACTIVE_SESSION_OPENED);
                } else {
                    postActiveSessionAction(Session.ACTION_ACTIVE_SESSION_CLOSED);
                }
            }
        }
    }

    private static void runWithHandlerOrExecutor(Handler handler, Runnable runnable) {
        if (handler != null) {
            handler.post(runnable);
        } else {
            //TODO: unimplemented
            Settings.getExecutor().execute(runnable);
        }
    }

    static void postActiveSessionAction(String action) {
        final Intent intent = new Intent(action);

        LocalBroadcastManager.getInstance(getStaticContext()).sendBroadcast(intent);
    }

    static Context getStaticContext() {
        return staticContext;
    }

    /**
     * Returns a boolean indicating whether the session is opened.
     *
     * @return a boolean indicating whether the session is opened.
     */
    public final boolean isOpened() {
        synchronized (this.lock) {
            return this.state.isOpened();
        }
    }

    public final boolean isClosed() {
        synchronized (this.lock) {
            return this.state.isClosed();
        }
    }

    /**
     * Returns the current state of the Session.
     * See {@link SessionState} for details.
     *
     * @return the current state of the Session.
     */
    public final SessionState getState() {
        synchronized (this.lock) {
            return this.state;
        }
    }

    /**
     * Returns the application id associated with this Session.
     *
     * @return the application id associated with this Session.
     */
    public final String getApplicationId() {
        return this.applicationId;
    }

    /**
     * Builder class used to create a Session.
     */
    public static final class Builder {
        private final Context context;
        private String applicationId;
        private TokenCachingStrategy tokenCachingStrategy;

        /**
         * Constructs a new Builder associated with the context.
         *
         * @param context the Activity or Service starting the Session
         */
        public Builder(Context context) {
            this.context = context;
        }

        /**
         * Sets the application id for the Session.
         *
         * @param applicationId the application id
         * @return the Builder instance
         */
        public Builder setApplicationId(final String applicationId) {
            this.applicationId = applicationId;
            return this;
        }

        /**
         * Sets the TokenCachingStrategy for the Session.
         *
         * @param tokenCachingStrategy the token cache to use
         * @return the Builder instance
         */
        public Builder setTokenCachingStrategy(final TokenCachingStrategy tokenCachingStrategy) {
            this.tokenCachingStrategy = tokenCachingStrategy;
            return this;
        }

        /**
         * Build the Session.
         *
         * @return a new Session
         */
        public Session build() {
            return new Session(context, applicationId, tokenCachingStrategy);
        }
    }

    interface StartActivityDelegate {
        public void startActivityForResult(Intent intent, int requestCode);

        public Activity getActivityContext();
    }

    public static class AuthorizationRequest implements Serializable {

        private static final long serialVersionUID = 1L;

        //TODO: Unimplemented
        private final StartActivityDelegate startActivityDelegate;
        private SessionLoginBehavior loginBehavior = SessionLoginBehavior.SSO_WITH_FALLBACK;
        private int requestCode = DEFAULT_AUTHORIZE_ACTIVITY_CODE;
        // This is the object that synchronizes access to state and tokenInfo
        private final Object lock = new Object();

        private String applicationId;
        private SessionState state;
        //TODO: unimplemented
//        private AccessToken tokenInfo;
        private Date lastAttemptedTokenExtendDate = new Date(0);

        private AuthorizationRequest pendingAuthorizationRequest;
        //TODO: unimplemented
//        private AuthorizationClient authorizationClient;

        //TODO: Unimplemented
        private StatusCallback statusCallback;
        private boolean isLegacy = false;
        private List<String> permissions = Collections.emptyList();
        private SessionDefaultAudience defaultAudience = SessionDefaultAudience.FRIENDS;
        private String validateSameFbidAsToken;
        private final String authId = UUID.randomUUID().toString();
        private final Map<String, String> loggingExtras = new HashMap<String, String>();

        AuthorizationRequest(final Activity activity) {
            startActivityDelegate = new StartActivityDelegate() {
                @Override
                public void startActivityForResult(Intent intent, int requestCode) {
                    activity.startActivityForResult(intent, requestCode);
                }

                @Override
                public Activity getActivityContext() {
                    return activity;
                }
            };
        }

        AuthorizationRequest(final Fragment fragment) {
            startActivityDelegate = new StartActivityDelegate() {
                @Override
                public void startActivityForResult(Intent intent, int requestCode) {
                    fragment.startActivityForResult(intent, requestCode);
                }

                @Override
                public Activity getActivityContext() {
                    return fragment.getActivity();
                }
            };
        }

        /**
         * Constructor to be used for V1 serialization only, DO NOT CHANGE.
         */
        private AuthorizationRequest(SessionLoginBehavior loginBehavior, int requestCode,
                                     List<String> permissions, String defaultAudience, boolean isLegacy, String applicationId,
                                     String validateSameFbidAsToken) {
            startActivityDelegate = new StartActivityDelegate() {
                @Override
                public void startActivityForResult(Intent intent, int requestCode) {
                    throw new UnsupportedOperationException(
                            "Cannot create an AuthorizationRequest without a valid Activity or Fragment");
                }

                @Override
                public Activity getActivityContext() {
                    throw new UnsupportedOperationException(
                            "Cannot create an AuthorizationRequest without a valid Activity or Fragment");
                }
            };
            this.loginBehavior = loginBehavior;
            this.requestCode = requestCode;
            this.permissions = permissions;
            this.defaultAudience = SessionDefaultAudience.valueOf(defaultAudience);
            this.isLegacy = isLegacy;
            this.applicationId = applicationId;
            this.validateSameFbidAsToken = validateSameFbidAsToken;
        }

        /**
         * Used for backwards compatibility with Facebook.java only, DO NOT USE.
         *
         * @param isLegacy
         */
        public void setIsLegacy(boolean isLegacy) {
            this.isLegacy = isLegacy;
        }

        boolean isLegacy() {
            return isLegacy;
        }

        //TODO: unimplemented
//        AuthorizationRequest setCallback(StatusCallback statusCallback) {
//            this.statusCallback = statusCallback;
//            return this;
//        }
        //TODO: unimplemented
//        StatusCallback getCallback() {
//            return statusCallback;
//        }

        AuthorizationRequest setLoginBehavior(SessionLoginBehavior loginBehavior) {
            if (loginBehavior != null) {
                this.loginBehavior = loginBehavior;
            }
            return this;
        }

        SessionLoginBehavior getLoginBehavior() {
            return loginBehavior;
        }

        AuthorizationRequest setRequestCode(int requestCode) {
            if (requestCode >= 0) {
                this.requestCode = requestCode;
            }
            return this;
        }

        int getRequestCode() {
            return requestCode;
        }

        AuthorizationRequest setPermissions(List<String> permissions) {
            if (permissions != null) {
                this.permissions = permissions;
            }
            return this;
        }

        AuthorizationRequest setPermissions(String... permissions) {
            return setPermissions(Arrays.asList(permissions));
        }

        List<String> getPermissions() {
            return permissions;
        }

        AuthorizationRequest setDefaultAudience(SessionDefaultAudience defaultAudience) {
            if (defaultAudience != null) {
                this.defaultAudience = defaultAudience;
            }
            return this;
        }

        SessionDefaultAudience getDefaultAudience() {
            return defaultAudience;
        }

        StartActivityDelegate getStartActivityDelegate() {
            return startActivityDelegate;
        }

        String getApplicationId() {
            return applicationId;
        }

        void setApplicationId(String applicationId) {
            this.applicationId = applicationId;
        }

        String getValidateSameFbidAsToken() {
            return validateSameFbidAsToken;
        }

        void setValidateSameFbidAsToken(String validateSameFbidAsToken) {
            this.validateSameFbidAsToken = validateSameFbidAsToken;
        }

        String getAuthId() {
            return authId;
        }

        /**
         * <p>
         * Logs a user in to Facebook.
         * </p>
         * <p>
         * A session may not be used with Request and other classes
         * in the SDK until it is open. If, prior to calling open, the session is in
         * the {@link SessionState#CREATED_TOKEN_LOADED CREATED_TOKEN_LOADED}
         * state, and the requested permissions are a subset of the previously authorized
         * permissions, then the Session becomes usable immediately with no user interaction.
         * </p>
         * <p>
         * The permissions associated with the openRequest passed to this method must
         * be read permissions only (or null/empty). It is not allowed to pass publish
         * permissions to this method and will result in an exception being thrown.
         * </p>
         * <p>
         * Any open method must be called at most once, and cannot be called after the
         * Session is closed. Calling the method at an invalid time will result in
         * UnsuportedOperationException.
         * </p>
         *
         * @param openRequest the open request, can be null only if the Session is in the
         *                    {@link SessionState#CREATED_TOKEN_LOADED CREATED_TOKEN_LOADED} state
         */
        public final void openForRead(OpenRequest openRequest) {
            open(openRequest, SessionAuthorizationType.READ);
        }

        private void open(OpenRequest openRequest, SessionAuthorizationType authType) {
            //TODO: unimplemented
//            validatePermissions(openRequest, authType);
//            validateLoginBehavior(openRequest);

            SessionState newState;
            synchronized (this.lock) {
                //TODO: unimplemented
//                if (pendingAuthorizationRequest != null) {
//                    postStateChange(state, state, new UnsupportedOperationException(
//                            "Session: an attempt was made to open a session that has a pending request."));
//                    return;
//                }
                final SessionState oldState = this.state;

                switch (this.state) {
                    case CREATED:
                        this.state = newState = SessionState.OPENING;
                        if (openRequest == null) {
                            throw new IllegalArgumentException("openRequest cannot be null when opening a new Session");
                        }
                        pendingAuthorizationRequest = openRequest;
                        break;
                    case CREATED_TOKEN_LOADED:
                        //TODO: unimplemented
                        if (openRequest != null) {
                            //TODO: unimplemented
                            pendingAuthorizationRequest = openRequest;
                        }
                        if (pendingAuthorizationRequest == null) {
                            this.state = newState = SessionState.OPENED;
                        } else {
                            this.state = newState = SessionState.OPENING;
                        }
                        break;
                    default:
                        throw new UnsupportedOperationException(
                                "Session: an attempt was made to open an already opened session.");
                }
                //TODO: unimplemented
//                if (openRequest != null) {
//                    addCallback(openRequest.getCallback());
//                }
//                this.postStateChange(oldState, newState, null);
            }

            if (newState == SessionState.OPENING) {
                authorize(openRequest);
            }
        }

        private boolean tryLoginActivity(AuthorizationRequest request) {
            Intent intent = getLoginActivityIntent(request);

            if (!resolveIntent(intent)) {
                return false;
            }

            try {
                request.getStartActivityDelegate().startActivityForResult(intent, request.getRequestCode());
            } catch (ActivityNotFoundException e) {
                return false;
            }

            return true;
        }

        private boolean resolveIntent(Intent intent) {
            ResolveInfo resolveInfo = getStaticContext().getPackageManager().resolveActivity(intent, 0);
            if (resolveInfo == null) {
                return false;
            }
            return true;
        }

        private Intent getLoginActivityIntent(AuthorizationRequest request) {
            Intent intent = new Intent();
            intent.setClass(getStaticContext(), LoginActivity.class);
            intent.setAction(request.getLoginBehavior().toString());

            // Let LoginActivity populate extras appropriately
            //TODO: unimplemented
//            AuthorizationClient.AuthorizationRequest authClientRequest = request.getAuthorizationClientRequest();
//            Bundle extras = LoginActivity.populateIntentExtras(authClientRequest);
//            intent.putExtras(extras);

            return intent;
        }

        private void tryLegacyAuth(final AuthorizationRequest request) {
            //TODO: unimplemented
//            authorizationClient = new AuthorizationClient();
//            authorizationClient.setOnCompletedListener(new AuthorizationClient.OnCompletedListener() {
//                @Override
//                public void onCompleted(AuthorizationClient.Result result) {
//                    int activityResult;
//                    if (result.code == AuthorizationClient.Result.Code.CANCEL) {
//                        activityResult = Activity.RESULT_CANCELED;
//                    } else {
//                        activityResult = Activity.RESULT_OK;
//                    }
//                    handleAuthorizationResult(activityResult, result);
//                }
//            });
//            authorizationClient.setContext(getStaticContext());
//            authorizationClient.startOrContinueAuth(request.getAuthorizationClientRequest());
        }

        void authorize(AuthorizationRequest request) {
            boolean started = false;

            request.setApplicationId(applicationId);

//            autoPublishAsync();

//            logAuthorizationStart();

            started = tryLoginActivity(request);

            //TODO: unimplemented
//            pendingAuthorizationRequest.loggingExtras.put(AuthorizationClient.EVENT_EXTRAS_TRY_LOGIN_ACTIVITY,
//                    started ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
//
            if (!started && request.isLegacy) {
//                pendingAuthorizationRequest.loggingExtras.put(AuthorizationClient.EVENT_EXTRAS_TRY_LEGACY,
//                        AppEventsConstants.EVENT_PARAM_VALUE_YES);

                tryLegacyAuth(request);
                started = true;
            }

            if (!started) {
                synchronized (this.lock) {
                    final SessionState oldState = this.state;

                    switch (this.state) {
                        case CLOSED:
                        case CLOSED_LOGIN_FAILED:
                            return;

                        default:
                            this.state = SessionState.CLOSED_LOGIN_FAILED;

                            //TODO: unimplemented
//                            Exception exception = new FacebookException(
//                                    "Log in attempt failed: LoginActivity could not be started, and not legacy request");
//                            logAuthorizationComplete(AuthorizationClient.Result.Code.ERROR, null, exception);
//                            postStateChange(oldState, this.state, exception);
                    }
                }
            }
        }

        //TODO: unimplemented !!!!
//        AuthorizationClient.AuthorizationRequest getAuthorizationClientRequest() {
//            AuthorizationClient.StartActivityDelegate delegate = new AuthorizationClient.StartActivityDelegate() {
//                @Override
//                public void startActivityForResult(Intent intent, int requestCode) {
//                    startActivityDelegate.startActivityForResult(intent, requestCode);
//                }
//
//                @Override
//                public Activity getActivityContext() {
//                    return startActivityDelegate.getActivityContext();
//                }
//            };
//            return new AuthorizationClient.AuthorizationRequest(loginBehavior, requestCode, isLegacy,
//                    permissions, defaultAudience, applicationId, validateSameFbidAsToken, delegate, authId);
//        }

        // package private so subclasses can use it
        Object writeReplace() {
            return new AuthRequestSerializationProxyV1(
                    loginBehavior, requestCode, permissions, defaultAudience.name(), isLegacy, applicationId, validateSameFbidAsToken);
        }

        // have a readObject that throws to prevent spoofing; must be private so serializer will call it (will be
        // called automatically prior to any base class)
        private void readObject(ObjectInputStream stream) throws InvalidObjectException {
            throw new InvalidObjectException("Cannot readObject, serialization proxy required");
        }

        private static class AuthRequestSerializationProxyV1 implements Serializable {
            private static final long serialVersionUID = -8748347685113614927L;
            private final SessionLoginBehavior loginBehavior;
            private final int requestCode;
            private boolean isLegacy;
            private final List<String> permissions;
            private final String defaultAudience;
            private final String applicationId;
            private final String validateSameFbidAsToken;

            private AuthRequestSerializationProxyV1(SessionLoginBehavior loginBehavior,
                                                    int requestCode, List<String> permissions, String defaultAudience, boolean isLegacy,
                                                    String applicationId, String validateSameFbidAsToken) {
                this.loginBehavior = loginBehavior;
                this.requestCode = requestCode;
                this.permissions = permissions;
                this.defaultAudience = defaultAudience;
                this.isLegacy = isLegacy;
                this.applicationId = applicationId;
                this.validateSameFbidAsToken = validateSameFbidAsToken;
            }

            private Object readResolve() {
                return new AuthorizationRequest(loginBehavior, requestCode, permissions, defaultAudience, isLegacy,
                        applicationId, validateSameFbidAsToken);
            }
        }
    }

    /**
     * A request used to open a Session.
     */
    public static final class OpenRequest extends AuthorizationRequest {
        private static final long serialVersionUID = 1L;

        /**
         * Constructs an OpenRequest.
         *
         * @param activity the Activity to use to open the Session
         */
        public OpenRequest(Activity activity) {
            super(activity);
        }

        /**
         * Constructs an OpenRequest.
         *
         * @param fragment the Fragment to use to open the Session
         */
        public OpenRequest(Fragment fragment) {
            super(fragment);
        }

        /**
         * Sets the StatusCallback for the OpenRequest.
         * <p/>
         * notify regarding Session state changes.
         *
         * @return the OpenRequest object to allow for chaining
         */
        //TODO: unimplemented
        public final OpenRequest setCallback() {
//            super.setCallback(statusCallback);
            return this;
        }

        /**
         * Sets the login behavior for the OpenRequest.
         *
         * @param loginBehavior The {@link SessionLoginBehavior SessionLoginBehavior} that
         *                      specifies what behaviors should be attempted during
         *                      authorization.
         * @return the OpenRequest object to allow for chaining
         */
        public final OpenRequest setLoginBehavior(SessionLoginBehavior loginBehavior) {
            super.setLoginBehavior(loginBehavior);
            return this;
        }

        /**
         * Sets the request code for the OpenRequest.
         *
         * @param requestCode An integer that identifies this request. This integer will be used
         *                    as the request code in {@link android.app.Activity#onActivityResult
         *                    onActivityResult}. This integer should be >= 0. If a value < 0 is
         *                    passed in, then a default value will be used.
         * @return the OpenRequest object to allow for chaining
         */
        public final OpenRequest setRequestCode(int requestCode) {
            super.setRequestCode(requestCode);
            return this;
        }

        /**
         * Sets the permissions for the OpenRequest.
         *
         * @param permissions A List&lt;String&gt; representing the permissions to request
         *                    during the authentication flow. A null or empty List
         *                    represents basic permissions.
         * @return the OpenRequest object to allow for chaining
         */
        public final OpenRequest setPermissions(List<String> permissions) {
            super.setPermissions(permissions);
            return this;
        }

        /**
         * Sets the permissions for the OpenRequest.
         *
         * @param permissions the permissions to request during the authentication flow.
         * @return the OpenRequest object to allow for chaining
         */
        public final OpenRequest setPermissions(String... permissions) {
            super.setPermissions(permissions);
            return this;
        }

        /**
         * Sets the defaultAudience for the OpenRequest.
         * <p/>
         * This is only used during Native login using a sufficiently recent facebook app.
         *
         * @param defaultAudience A SessionDefaultAudience representing the default audience setting to request.
         * @return the OpenRequest object to allow for chaining
         */
        public final OpenRequest setDefaultAudience(SessionDefaultAudience defaultAudience) {
            super.setDefaultAudience(defaultAudience);
            return this;
        }

//        StatusCallback getCallback() {
//            return statusCallback;
//        }
    }

    /**
     * <p>
     * Logs a user in to Facebook.
     * </p>
     * <p>
     * A session may not be used Request and other classes
     * in the SDK until it is open. If, prior to calling open, the session is in
     * the {@link SessionState#CREATED_TOKEN_LOADED CREATED_TOKEN_LOADED}
     * state, and the requested permissions are a subset of the previously authorized
     * permissions, then the Session becomes usable immediately with no user interaction.
     * </p>
     * <p>
     * The permissions associated with the openRequest passed to this method must
     * be read permissions only (or null/empty). It is not allowed to pass publish
     * permissions to this method and will result in an exception being thrown.
     * </p>
     * <p>
     * Any open method must be called at most once, and cannot be called after the
     * Session is closed. Calling the method at an invalid time will result in
     * UnsuportedOperationException.
     * </p>
     *
     * @param openRequest the open request, can be null only if the Session is in the
     *                    {@link SessionState#CREATED_TOKEN_LOADED CREATED_TOKEN_LOADED} state
     */
    public final void openForRead(OpenRequest openRequest) {
        open(openRequest, SessionAuthorizationType.READ);
    }

    /**
     * <p>
     * Logs a user in to Facebook.
     * </p>
     * <p>
     * A session may not be used with Request and other classes
     * in the SDK until it is open. If, prior to calling open, the session is in
     * the {@link SessionState#CREATED_TOKEN_LOADED CREATED_TOKEN_LOADED}
     * state, and the requested permissions are a subset of the previously authorized
     * permissions, then the Session becomes usable immediately with no user interaction.
     * </p>
     * <p>
     * The permissions associated with the openRequest passed to this method must
     * be publish or manage permissions only and must be non-empty. Any read permissions
     * will result in a warning, and may fail during server-side authorization. Also, an application
     * must have at least basic read permissions prior to requesting publish permissions, so
     * this method should only be used if the application knows that the user has already granted
     * read permissions to the application; otherwise, openForRead should be used, followed by a
     * call to requestNewPublishPermissions. For more information on this flow, see
     * https://developers.facebook.com/docs/facebook-login/permissions/.
     * </p>
     * <p>
     * Any open method must be called at most once, and cannot be called after the
     * Session is closed. Calling the method at an invalid time will result in
     * UnsuportedOperationException.
     * </p>
     *
     * @param openRequest the open request, can be null only if the Session is in the
     *                    {@link SessionState#CREATED_TOKEN_LOADED CREATED_TOKEN_LOADED} state
     */
    public final void openForPublish(OpenRequest openRequest) {
        open(openRequest, SessionAuthorizationType.PUBLISH);
    }

    /**
     * Opens a session based on an existing Facebook access token. This method should be used
     * only in instances where an application has previously obtained an access token and wishes
     * to import it into the Session/TokenCachingStrategy-based session-management system. An
     * example would be an application which previously did not use the Facebook SDK for Android
     * and implemented its own session-management scheme, but wishes to implement an upgrade path
     * for existing users so they do not need to log in again when upgrading to a version of
     * the app that uses the SDK.
     * <p/>
     * No validation is done that the token, token source, or permissions are actually valid.
     * It is the caller's responsibility to ensure that these accurately reflect the state of
     * the token that has been passed in, or calls to the Facebook API may fail.
     *
     * @param accessToken the access token obtained from Facebook
     * @param callback    a callback that will be called when the session status changes; may be null
     */
    public final void open(AccessToken accessToken, StatusCallback callback) {
        synchronized (this.lock) {
            if (pendingAuthorizationRequest != null) {
                throw new UnsupportedOperationException(
                        "Session: an attempt was made to open a session that has a pending request.");
            }

            if (state.isClosed()) {
                throw new UnsupportedOperationException(
                        "Session: an attempt was made to open a previously-closed session.");
            } else if (state != SessionState.CREATED && state != SessionState.CREATED_TOKEN_LOADED) {
                throw new UnsupportedOperationException(
                        "Session: an attempt was made to open an already opened session.");
            }
            if (callback != null) {
                addCallback(callback);
            }
            this.tokenInfo = accessToken;
            if (this.tokenCachingStrategy != null) {
                this.tokenCachingStrategy.save(accessToken.toCacheBundle());
            }

            final SessionState oldState = state;
            state = SessionState.OPENED;
            this.postStateChange(oldState, state, null);
        }

        //TODO: unimplemented
//        autoPublishAsync();
    }

    private void open(OpenRequest openRequest, SessionAuthorizationType authType) {
        //TODO: unimplemented
//        validatePermissions(openRequest, authType);
//        validateLoginBehavior(openRequest);

        SessionState newState;
        synchronized (this.lock) {
            if (pendingAuthorizationRequest != null) {
                postStateChange(state, state, new UnsupportedOperationException(
                        "Session: an attempt was made to open a session that has a pending request."));
                return;
            }
            final SessionState oldState = this.state;

            switch (this.state) {
                case CREATED:
                    this.state = newState = SessionState.OPENING;
                    if (openRequest == null) {
                        throw new IllegalArgumentException("openRequest cannot be null when opening a new Session");
                    }
                    pendingAuthorizationRequest = openRequest;
                    break;
                case CREATED_TOKEN_LOADED:
                    //TODO: unimplemented
//                    if (openRequest != null && !Utility.isNullOrEmpty(openRequest.getPermissions())) {
//                        if (!Utility.isSubset(openRequest.getPermissions(), getPermissions())) {
//                            pendingAuthorizationRequest = openRequest;
//                        }
//                    }

                    if (pendingAuthorizationRequest == null) {
                        this.state = newState = SessionState.OPENED;
                    } else {
                        this.state = newState = SessionState.OPENING;
                    }
                    break;
                default:
                    throw new UnsupportedOperationException(
                            "Session: an attempt was made to open an already opened session.");
            }
            //TODO: unimplemented
//            if (openRequest != null) {
//                addCallback(openRequest.getCallback());
//            }
            this.postStateChange(oldState, newState, null);
        }

        if (newState == SessionState.OPENING) {
            authorize(openRequest);
        }
    }

    StatusCallback getCallback() {
        return statusCallback;
    }

    private void tryLegacyAuth(final AuthorizationRequest request) {
        //TODO: unimplemented
//        authorizationClient = new AuthorizationClient();
//        authorizationClient.setOnCompletedListener(new AuthorizationClient.OnCompletedListener() {
//            @Override
//            public void onCompleted(AuthorizationClient.Result result) {
//                int activityResult;
//                if (result.code == AuthorizationClient.Result.Code.CANCEL) {
//                    activityResult = Activity.RESULT_CANCELED;
//                } else {
//                    activityResult = Activity.RESULT_OK;
//                }
//                handleAuthorizationResult(activityResult, result);
//            }
//        });
//        authorizationClient.setContext(getStaticContext());
//        authorizationClient.startOrContinueAuth(request.getAuthorizationClientRequest());
    }

    void authorize(AuthorizationRequest request) {
        boolean started = false;

        request.setApplicationId(applicationId);
//TODO: unimplemented
//        autoPublishAsync();
//
//        logAuthorizationStart();

        started = tryLoginActivity(request);
//TODO: unimplemented
//        pendingAuthorizationRequest.loggingExtras.put(AuthorizationClient.EVENT_EXTRAS_TRY_LOGIN_ACTIVITY,
//                started ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO);
//
//        if (!started && request.isLegacy) {
//            pendingAuthorizationRequest.loggingExtras.put(AuthorizationClient.EVENT_EXTRAS_TRY_LEGACY,
//                    AppEventsConstants.EVENT_PARAM_VALUE_YES);

//        tryLegacyAuth(request);
//        started = true;
//        }

        if (!started) {
            synchronized (this.lock) {
                final SessionState oldState = this.state;

                switch (this.state) {
                    case CLOSED:
                    case CLOSED_LOGIN_FAILED:
                        return;

                    default:
                        this.state = SessionState.CLOSED_LOGIN_FAILED;

                        //TODO: unimplemented
//                        Exception exception = new FacebookException(
//                                "Log in attempt failed: LoginActivity could not be started, and not legacy request");
//                        logAuthorizationComplete(AuthorizationClient.Result.Code.ERROR, null, exception);
//                        postStateChange(oldState, this.state, exception);
                }
            }
        }
    }

    private boolean resolveIntent(Intent intent) {
        ResolveInfo resolveInfo = getStaticContext().getPackageManager().resolveActivity(intent, 0);
        if (resolveInfo == null) {
            return false;
        }
        return true;
    }

    private Intent getLoginActivityIntent(AuthorizationRequest request) {
        Intent intent = new Intent();
        intent.setClass(getStaticContext(), LoginActivity.class);
        intent.setAction(request.getLoginBehavior().toString());

        // Let LoginActivity populate extras appropriately
        //TODO: unimplemented
//        AuthorizationClient.AuthorizationRequest authClientRequest = request.getAuthorizationClientRequest();
//        Bundle extras = LoginActivity.populateIntentExtras(authClientRequest);
//        intent.putExtras(extras);

        return intent;
    }

    private boolean tryLoginActivity(AuthorizationRequest request) {
        Intent intent = getLoginActivityIntent(request);

//        if (!resolveIntent(intent)) {
//            return false;
//        }

//        try {
        request.getStartActivityDelegate().startActivityForResult(intent, request.getRequestCode());
//        } catch (ActivityNotFoundException e) {
//            return false;
//        }

        return true;
    }

    /**
     * Provides an implementation for {@link android.app.Activity#onActivityResult
     * onActivityResult} that updates the Session based on information returned
     * during the authorization flow. The Activity that calls open or
     * requestNewPermissions should forward the resulting onActivityResult call here to
     * update the Session state based on the contents of the resultCode and
     * data.
     *
     * @param currentActivity The Activity that is forwarding the onActivityResult call.
     * @param requestCode     The requestCode parameter from the forwarded call. When this
     *                        onActivityResult occurs as part of Facebook authorization
     *                        flow, this value is the activityCode passed to open or
     *                        authorize.
     * @param resultCode      An int containing the resultCode parameter from the forwarded
     *                        call.
     * @param data            The Intent passed as the data parameter from the forwarded
     *                        call.
     * @return A boolean indicating whether the requestCode matched a pending
     * authorization request for this Session.
     */
    public final boolean onActivityResult(Activity currentActivity, int requestCode, int resultCode, Intent data) {
        //TODO: unimplemented
//        Validate.notNull(currentActivity, "currentActivity");

        initializeStaticContext(currentActivity);

        synchronized (lock) {
            if (pendingAuthorizationRequest == null || (requestCode != pendingAuthorizationRequest.getRequestCode())) {
                return false;
            }
        }

        Exception exception = null;
        AuthorizationClient.Result.Code code = AuthorizationClient.Result.Code.ERROR;

        if (data != null) {
//            AuthorizationClient.Result result = (AuthorizationClient.Result) data.getSerializableExtra(
//                    LoginActivity.RESULT_KEY);
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                // This came from LoginActivity.
                handleAuthorizationResult(resultCode, bundle);
                return true;
            }
//            else if (authorizationClient != null) {
//                // Delegate to the auth client.
//                authorizationClient.onActivityResult(requestCode, resultCode, data);
//                return true;
//            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            //TODO: unimplemented
//            exception = new FacebookOperationCanceledException("User canceled operation.");
            code = AuthorizationClient.Result.Code.CANCEL;
        }

        if (exception == null) {
            //TODO: unimplemented
//            exception = new FacebookException("Unexpected call to Session.onActivityResult");
        }

        logAuthorizationComplete(code, null, exception);
        finishAuthOrReauth(null, exception);

        return true;
    }

    void finishAuthOrReauth(AccessToken newToken, Exception exception) {
        // If the token we came up with is expired/invalid, then auth failed.
        //TODO: unimplemented
//        if ((newToken != null) && newToken.isInvalid()) {
//            newToken = null;
//            exception = new FacebookException("Invalid access token.");
//        }


        synchronized (this.lock) {
            switch (this.state) {
                case OPENING:
                    // This means we are authorizing for the first time in this Session.
                    finishAuthorization(newToken, exception);
                    break;

                case OPENED:
                case OPENED_TOKEN_UPDATED:
                    // This means we are reauthorizing.
                    finishReauthorization(newToken, exception);
                    break;

                case CREATED:
                case CREATED_TOKEN_LOADED:
                case CLOSED:
                case CLOSED_LOGIN_FAILED:
                    Log.d(TAG, "Unexpected call to finishAuthOrReauth in state " + this.state);
                    break;
            }
        }
    }

    private void finishAuthorization(AccessToken newToken, Exception exception) {
        final SessionState oldState = state;
        if (newToken != null) {
            tokenInfo = newToken;
            saveTokenToCache(newToken);

            state = SessionState.OPENED;
        } else if (exception != null) {
            state = SessionState.CLOSED_LOGIN_FAILED;
        }
        pendingAuthorizationRequest = null;
        postStateChange(oldState, state, exception);
    }

    private void saveTokenToCache(AccessToken newToken) {
        if (newToken != null && tokenCachingStrategy != null) {
            tokenCachingStrategy.save(newToken.toCacheBundle());
        }
    }

    private void finishReauthorization(final AccessToken newToken, Exception exception) {
        final SessionState oldState = state;

        if (newToken != null) {
            tokenInfo = newToken;
            saveTokenToCache(newToken);

            state = SessionState.OPENED_TOKEN_UPDATED;
        }

        pendingAuthorizationRequest = null;
//        postStateChange(oldState, state, exception);
    }

    private void logAuthorizationComplete(AuthorizationClient.Result.Code result, Map<String, String> resultExtras,
                                          Exception exception) {
        Bundle bundle = null;
        if (pendingAuthorizationRequest == null) {
            // We don't expect this to happen, but if it does, log an event for diagnostic purposes.
            bundle = AuthorizationClient.newAuthorizationLoggingBundle("");
            bundle.putString(AuthorizationClient.EVENT_PARAM_LOGIN_RESULT,
                    AuthorizationClient.Result.Code.ERROR.getLoggingValue());
            bundle.putString(AuthorizationClient.EVENT_PARAM_ERROR_MESSAGE,
                    "Unexpected call to logAuthorizationComplete with null pendingAuthorizationRequest.");
        } else {
            bundle = AuthorizationClient.newAuthorizationLoggingBundle(pendingAuthorizationRequest.getAuthId());
            if (result != null) {
                bundle.putString(AuthorizationClient.EVENT_PARAM_LOGIN_RESULT, result.getLoggingValue());
            }
            if (exception != null && exception.getMessage() != null) {
                bundle.putString(AuthorizationClient.EVENT_PARAM_ERROR_MESSAGE, exception.getMessage());
            }

            // Combine extras from the request and from the result.
            JSONObject jsonObject = null;
            if (pendingAuthorizationRequest.loggingExtras.isEmpty() == false) {
                jsonObject = new JSONObject(pendingAuthorizationRequest.loggingExtras);
            }
            if (resultExtras != null) {
                if (jsonObject == null) {
                    jsonObject = new JSONObject();
                }
                try {
                    for (Map.Entry<String, String> entry : resultExtras.entrySet()) {
                        jsonObject.put(entry.getKey(), entry.getValue());
                    }
                } catch (JSONException e) {
                }
            }
            if (jsonObject != null) {
                bundle.putString(AuthorizationClient.EVENT_PARAM_EXTRAS, jsonObject.toString());
            }
        }
        bundle.putLong(AuthorizationClient.EVENT_PARAM_TIMESTAMP, System.currentTimeMillis());

        //TODO: unimplemented
//        AppEventsLogger logger = getAppEventsLogger();
//        logger.logSdkEvent(AuthorizationClient.EVENT_NAME_LOGIN_COMPLETE, null, bundle);
    }

    private void handleAuthorizationResult(int resultCode, Bundle bundle) {
        AccessToken newToken = null;
        Exception exception = null;
        if (resultCode == Activity.RESULT_OK) {
//            if (result.code == AuthorizationClient.Result.Code.SUCCESS) {
//                newToken = result.token;
            String token = bundle.getString("token");
            long userId = bundle.getLong("user_id");

            newToken = new AccessToken(token, userId);
//            } else {
            //TODO: unimplemented
//                exception = new FacebookAuthorizationException(result.errorMessage);
//                exception = new RuntimeException();
//            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            //TODO: unimplemented
//            exception = new FacebookOperationCanceledException(result.errorMessage);
            exception = new RuntimeException();
        }

//        logAuthorizationComplete(result.code, result.loggingExtras, exception);

        authorizationClient = null;
        finishAuthOrReauth(newToken, exception);
    }

    /**
     * Adds a callback that will be called when the state of this Session changes.
     *
     * @param callback the callback
     */
    public final void addCallback(StatusCallback callback) {
        synchronized (callbacks) {
            if (callback != null && !callbacks.contains(callback)) {
                callbacks.add(callback);
            }
        }
    }
}