package com.despair.simple.vk;

/**
 * Created by DESPAIR on 1/14/14.
 */

/**
 * com.facebook.internal is solely for the use of other packages within the Facebook SDK for Android. Use of
 * any of the classes in this package is unsupported, and they may be modified or removed without warning at
 * any time.
 */
public enum SessionAuthorizationType {
    READ,
    PUBLISH
}