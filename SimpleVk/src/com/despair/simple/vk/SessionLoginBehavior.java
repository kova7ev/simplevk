package com.despair.simple.vk;

/**
 * Created by DESPAIR on 1/14/14.
 */
/**
 * Specifies the behaviors to try during
 * {@link Session#openForRead(com.despair.simple.vk.facebook.Session.OpenRequest) openForRead},
 * {@link Session#openForPublish(com.despair.simple.vk.facebook.Session.OpenRequest) openForPublish},
 * {@link Session#requestNewReadPermissions(com.despair.simple.vk.facebook.Session.NewPermissionsRequest) requestNewReadPermissions}, or
 * {@link Session#requestNewPublishPermissions(com.despair.simple.vk.facebook.Session.NewPermissionsRequest) requestNewPublishPermissions}.
 */
public enum SessionLoginBehavior {
    /**
     * Specifies that Session should attempt Single Sign On (SSO), and if that
     * does not work fall back to dialog auth. This is the default behavior.
     */
    SSO_WITH_FALLBACK(true, true),

    /**
     * Specifies that Session should only attempt SSO. If SSO fails, then the
     * open or new permissions call fails.
     */
    SSO_ONLY(true, false),

    /**
     * Specifies that SSO should not be attempted, and to only use dialog auth.
     */
    SUPPRESS_SSO(false, true);

    private final boolean allowsKatanaAuth;
    private final boolean allowsWebViewAuth;

    private SessionLoginBehavior(boolean allowsKatanaAuth, boolean allowsWebViewAuth) {
        this.allowsKatanaAuth = allowsKatanaAuth;
        this.allowsWebViewAuth = allowsWebViewAuth;
    }

    public boolean allowsKatanaAuth() {
        return allowsKatanaAuth;
    }

    public boolean allowsWebViewAuth() {
        return allowsWebViewAuth;
    }
}