package com.despair.simple.vk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.perm.kate.api.Api;

/**
 * Created by DESPAIR on 1/14/14.
 */
public class SimpleVk {
    private Api mApi;
    private static SimpleVk mInstance;
    private static Activity mActivity;
    private static SimpleVkConfiguration mConfiguration;
    private SessionStatusCallback mSessionStatusCallback = null;

    public static void initialize(Activity activity) {
        if (mInstance == null) {
            mInstance = new SimpleVk();
        }

        mActivity = activity;
    }

    public static void setConfiguration(SimpleVkConfiguration vkToolsConfiguration) {
        mConfiguration = vkToolsConfiguration;
    }

    public static SimpleVk getInstance(Activity activity) {
        if (mInstance == null) {
            mInstance = new SimpleVk();
        }

        mActivity = activity;
        return mInstance;
    }

    public static SimpleVk getInstance() {
        return mInstance;
    }

    public SimpleVk(){
        mSessionStatusCallback = new SessionStatusCallback();
    }

    public boolean isLogin() {
        Session session = Session.getActiveSession();
        if (session == null) {
            if (mActivity == null) {
                // You can't create a session if the activity/context hasn't been initialized
                // This is now possible because the library can be started without context.
                return false;
            }
            session = new Builder(mActivity.getApplicationContext())
                    .setApplicationId(mConfiguration.getAppId())
                    .build();
            Session.setActiveSession(session);
        }
        if (session.isOpened()) {
            // mSessionNeedsToReopen = false;
            return true;
        }

		/*
         * Check if we can reload the session when it will be neccesary. We won't do it now.
		 */
        //TODO: Unimplemented
//        if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED))
//        {
//            List<String> permissions = session.getPermissions();
//            if (permissions.containsAll(mConfiguration.getReadPermissions()))
//            {
//                reopenSession();
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//        }
//        else
//        {
//            return false;
//        }
        return false;
    }

    public void login(OnLoginListener onLoginListener) {
        if (isLogin()) {
            //TODO: unimplemented
            // log
//            logInfo("You were already logged in before calling 'login()' method");
//
            if (onLoginListener != null) {
                onLoginListener.onLogin();
            }
        } else {
            Session session = Session.getActiveSession();
            if (session == null || session.getState().isClosed()) {
                session = new Session.Builder(mActivity.getApplicationContext())
                        .setApplicationId(mConfiguration.getAppId())
                        .build();
                Session.setActiveSession(session);
            }

            mSessionStatusCallback.mOnLoginListener = onLoginListener;
            session.addCallback(mSessionStatusCallback);

			/*
             * If session is not opened, then open it
			 */
            if (!session.isOpened()) {
                openSession(session, true);
            } else {
                if (onLoginListener != null) {
                    onLoginListener.onLogin();
                }
            }
        }
    }

    public void publish(final Photo photo, final OnPublishListener onPublishListener) {
        if (isLogin()) {
            // callback with 'thinking'
            if (onPublishListener != null) {
                onPublishListener.onThinking();
            }
            publishImpl(photo, onPublishListener);
        } else {
            // callback with 'fail' due to not being loged
            if (onPublishListener != null) {
                onPublishListener.onFail("You are not logged in");
            }
        }
    }

    private void publishImpl(Photo photo, final OnPublishListener onPublishListener) {
        if (mApi == null){
            Session session = Session.getActiveSession();
            AccessToken tokenInfo = session.getAccessToken();
            mApi = new Api(tokenInfo.getToken(), session.getApplicationId());
        }

        VkPostTask task = new VkPostTask();
        task.api = mApi;
        task.userId = Session.getActiveSession().getAccessToken().getUserId();
        task.context = mActivity.getApplicationContext();
        task.delegate = new VkPostResponse() {
            @Override
            public void onComplete() {
                if (onPublishListener != null) {
                    onPublishListener.onComplete("image post success");
                }
            }
        };
        task.execute(photo);
    }

    private void openSession(Session session, boolean isRead) {
        Session.OpenRequest request = new Session.OpenRequest(mActivity);
        if (request != null) {
            request.setDefaultAudience(mConfiguration.getSessionDefaultAudience());
            request.setLoginBehavior(mConfiguration.getSessionLoginBehavior());

            if (isRead) {
                //TODO: unimplemented
//                request.setPermissions(mConfiguration.getReadPermissions());

				/*
                 * In case there are also PUBLISH permissions, then we would ask for these permissions second
				 * time (after, user accepted the read permissions)
				 */
                //TODO: unimplemented
//                if (mConfiguration.hasPublishPermissions())
//                {
//                    mSessionStatusCallback.askPublishPermissions();
//                }

                // Open session with read permissions
                session.openForRead(request);
            } else {
                //TODO: unimplemented
//                request.setPermissions(mConfiguration.getPublishPermissions());
                session.openForPublish(request);
            }
        }
    }


    /**
     * Call this inside your activity in {@link android.app.Activity#onActivityResult} method
     *
     * @param activity
     * @param requestCode
     * @param resultCode
     * @param data
     * @return
     */
    public boolean onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (Session.getActiveSession() != null) {
            return Session.getActiveSession().onActivityResult(activity, requestCode, resultCode, data);
        } else {
            return false;
        }
    }

    class Builder {
        private String applicationId;
        private Context context;
        private TokenCachingStrategy tokenCachingStrategy;


        public Builder(Context context) {
            this.context = context;
        }

        public Builder setApplicationId(String appId) {
            this.applicationId = appId;
            return this;
        }

        public Session build() {
            return new Session(context, applicationId, tokenCachingStrategy);
        }
    }

    class SessionStatusCallback implements Session.StatusCallback {
        private boolean mAskPublishPermissions = false;
        private boolean mDoOnLogin = false;
        OnLoginListener mOnLoginListener = null;
        OnLogoutListener mOnLogoutListener = null;
        OnReopenSessionListener mOnReopenSessionListener = null;

        @Override
        public void call(Session session, SessionState state, Exception exception) {
            /*
             * These are already authorized permissions
			 */
//            List<String> permissions = session.getPermissions();

            //TODO: unimplemented
//            if (exception != null)
//            {
//                // log
////                logError("SessionStatusCallback: exception=", exception);
//
//                if (exception instanceof FacebookOperationCanceledException)
//                {
//					/*
//					 * If user canceled the read permissions dialog
//					 */
//                    if (permissions.size() == 0)
//                    {
//                        mOnLoginListener.onNotAcceptingPermissions();
//                    }
//                    else
//                    {
//						/*
//						 * User canceled the WRITE permissions. We do nothing here. Once the user will try to
//						 * do some action that require WRITE permissions, the dialog will be shown
//						 * automatically.
//						 */
//                    }
//                }
//                else
//                {
//                    mOnLoginListener.onException(exception);
//                }
//            }

            // log
//            logInfo("SessionStatusCallback: state=" + state.name() + ", session=" + String.valueOf(session));

            switch (state) {
                case CLOSED:
                    if (mOnLogoutListener != null) {
                        mOnLogoutListener.onLogout();
                    }
                    break;

                case CLOSED_LOGIN_FAILED:
                    break;

                case CREATED:
                    break;

                case CREATED_TOKEN_LOADED:
                    break;

                case OPENING:
                    if (mOnLoginListener != null) {
                        mOnLoginListener.onThinking();
                    }
                    break;

                case OPENED:

				/*
				 * Check if we came from publishing actions where we ask again for publish permissions
				 */
                    if (mOnReopenSessionListener != null) {
                        mOnReopenSessionListener.onNotAcceptingPermissions();
                        mOnReopenSessionListener = null;
                    }

				/*
				 * Check if WRITE permissions were also defined in the configuration. If so, then ask in
				 * another dialog for WRITE permissions.
				 */
                    else if (mAskPublishPermissions && session.getState().equals(SessionState.OPENED)) {
                        if (mDoOnLogin) {
						/*
						 * If user didn't accept the publish permissions, we still want to notify about
						 * complete
						 */
                            mDoOnLogin = false;
                            mOnLoginListener.onLogin();
                        } else {

                            mDoOnLogin = true;
                            //TODO: unimplemented
//                            extendPublishPermissions();
                            mAskPublishPermissions = false;
                        }
                    } else {
                        if (mOnLoginListener != null) {
                            mOnLoginListener.onLogin();
                        }
                    }
                    break;

                case OPENED_TOKEN_UPDATED:

				/*
				 * Check if came from publishing actions and we need to reask for publish permissions
				 */
                    if (mOnReopenSessionListener != null) {
                        mOnReopenSessionListener.onSuccess();
                        mOnReopenSessionListener = null;
                    } else if (mDoOnLogin) {
                        mDoOnLogin = false;

                        if (mOnLoginListener != null) {
                            mOnLoginListener.onLogin();
                        }
                    }

                    break;

                default:
                    break;
            }
        }

        /**
         * If we want to open another dialog with publish permissions just after showing read permissions,
         * then this method should be called
         */
        public void askPublishPermissions() {
            mAskPublishPermissions = true;
        }
    }

    public interface OnPublishListener extends OnActionListener {
        void onComplete(String id);
    }

    public interface OnLoginListener extends OnActionListener {
        /**
         * If user performed login(Activity) action, this callback method will be
         * invoked
         */
        void onLogin();

        /**
         * If user pressed 'cancel' in READ (First) permissions dialog
         */
        void onNotAcceptingPermissions();
    }

    public interface OnLogoutListener extends OnActionListener {
        /**
         * If user performed logout() action, this callback method will be invoked
         */
        void onLogout();
    }

    private interface OnReopenSessionListener {
        void onSuccess();

        void onNotAcceptingPermissions();
    }

    public interface OnActionListener extends OnErrorListener {
        void onThinking();
    }

    public interface OnErrorListener {
        void onException(Throwable throwable);

        void onFail(String reason);
    }

}