package com.despair.simple.vk;

/**
 * Created by DESPAIR on 1/14/14.
 */
public class SimpleVkConfiguration {
    private String mAppId;
    private SessionDefaultAudience mDefaultAudience = null;
    private SessionLoginBehavior mLoginBehavior = null;
    private boolean mHasPublishPermissions = false;

    public SimpleVkConfiguration(Builder builder) {
        this.mAppId = builder.mAppId;
    }

    public String getAppId() {
        return mAppId;
    }

    /**
     * Return <code>True</code> if 'PUBLISH' permissions are defined
     *
     * @return
     */
    boolean hasPublishPermissions()
    {
        return mHasPublishPermissions;
    }

    /**
     * Get session login behavior
     *
     * @return
     */
    SessionLoginBehavior getSessionLoginBehavior()
    {
        return mLoginBehavior;
    }

    /**
     * Get session default audience
     *
     * @return
     */
    SessionDefaultAudience getSessionDefaultAudience()
    {
        return mDefaultAudience;
    }

    public static class Builder{
        private String mAppId;

        public Builder(){

        }

        public Builder setAppId(String appId)
        {
            mAppId = appId;
            return this;
        }

        public SimpleVkConfiguration build()
        {
            return new SimpleVkConfiguration(this);
        }

    }
}
