package com.despair.simple.vk;

import android.content.Context;
import android.os.AsyncTask;
import com.perm.kate.api.Api;
import com.perm.kate.api.KException;
import com.perm.kate.api.Photo;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by DESPAIR on 1/4/14.
 */
public class VkPostTask extends AsyncTask<com.despair.simple.vk.Photo, Void, Void> {
    public VkPostResponse delegate;
    public Api api;
    public long userId;
    public Context context;

    private String filePath = "vkPhoto.bmp";

    @Override
    protected Void doInBackground(com.despair.simple.vk.Photo... photos) {
        try {
            com.despair.simple.vk.Photo photo = photos[0];
            saveFile(photo.getBytes());
            String server = api.photosGetWallUploadServer (userId, null);
            savePost(server, photo.getDescription());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (KException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        delegate.onComplete();
    }

    private Photo uploadPhotoToWall(String uploadServer) {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(uploadServer);
            MultipartEntity albumArtEntity = new MultipartEntity();
            File file = new File(context.getFilesDir(), filePath);
            albumArtEntity.addPart("photo", new FileBody(file));
            httpPost.setEntity(albumArtEntity);
            HttpResponse response = client.execute(httpPost);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            StringBuilder builder = new StringBuilder();

            for (String line; (line = reader.readLine()) != null; ) {
                builder.append(line).append("\n");
            }

            JSONObject photoObject = new JSONObject(builder.toString());

            return api.saveWallPhoto(photoObject.get("server").toString(),
                    photoObject.get("photo").toString(),
                    photoObject.get("hash").toString(),
                    userId,
                    null).get(0);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (KException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void savePost(String uploadServer, String message) {
        long photoPid = uploadPhotoToWall(uploadServer).pid;
        Collection<String> attachments = new ArrayList<String>();
        String att = String.format("photo%d_%d", userId, photoPid);
        attachments.add(att);

        try {
            api.createWallPost(userId, message, attachments, null, false, false, false, null, null, null, null, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (KException e) {
            e.printStackTrace();
        }
    }

    private void saveFile(byte[] bytes) {
        try {
            File file = new File(context.getFilesDir(), filePath);
            FileOutputStream fw = new FileOutputStream(file);
            fw.write(bytes);
            fw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}